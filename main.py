
while True:
    user_option = input("Type add, show, edit, complete or exit: ")
    user_option = user_option.strip()

    if 'add' in user_option:
        todo = user_option[4:]
        with open('todos.txt', 'r') as files:
            todos = files.readlines()

        todos.append(todo)

        with open('todos.txt', 'w') as files:
            files.writelines(todos)

    # case 'show' | 'display':
    if'show' in user_option:

        with open('todos.txt', 'r') as files:
            todos = files.readlines()

        for index, item in enumerate(todos):
            index += 1
            # item = item.title()
            # print(index, '.', item)
            item = item.strip('\n')
            row = f"{index}.{item}"
            print(row)
    if 'edit' in user_option:

        with open ('todos.txt', 'r') as files:
            todos = files.readlines()

        print('existing todos', todos)
        number = int(input("Enter the number of todo to edit: "))
        number -= 1

        new_todo = input("Enter new todo: ")
        todos[number] = new_todo + '\n'

        print('new todos ', todos)
        with open ('todos.txt', 'w') as files:
            todos = files.writelines(todos)

    if 'complete' in user_option:
        with open ('todos.txt', 'r') as files:
            todos = files.readlines()

        print('existing todos', todos)

        number = int(input("Enter the number of todo to complete: "))
        completed_task = todos[number - 1]
        todos.pop(number - 1)

        with open ('todos.txt', 'w') as files:
            todos = files.writelines(todos)

        print (f"The task {completed_task} has been removed from your list.\nCongrats!!")

    if 'exit' in user_option:
        print("Have a rocking day!")
        break


